class Person {
  constructor(name, age, sex, salary) {
    this.name = name;
    this.age = age;
    this.sex = sex;
    this.salary = salary;
  }

  static sort(array, field, order) {
       //////////////////` main
   let arr = [...array];
    let n = arr.length;
    if (Object.keys(arr[0]).indexOf(field) !== -1 &&(order === "asc" || order === "desc")) {
      quickSort(arr, 0, n - 1, field, order);
      return arr;
    } 
    else {
      return alert("order or field wrong");
    }
    ////////////////////// quicksort functions 
    
    function swap(arr, i, j) {
      let temp = arr[i];
      arr[i] = arr[j];
      arr[j] = temp;
    }

    function partition(arr, low, high, field, order) {
      let pivot = arr[high];
      let i = low - 1;
      for (let j = low; j <= high - 1; j++) {
        if (
          (order == "asc" && arr[j][field] < pivot[field]) ||
          (order == "desc" && arr[j][field] > pivot[field])
        ) {
          i++;
          swap(arr, i, j);
        }
      }
      swap(arr, i + 1, high);
      return i + 1;
    }

    function quickSort(arr, low, high, field, order) {
      if (low < high) {
        let pi = partition(arr, low, high, field, order);
        quickSort(arr, low, pi - 1, field, order);
        quickSort(arr, pi + 1, high, field, order);
      }
    }
  }
}

const array = [
  new Person("harsh", 12, "male", 3000),
  new Person("blakh", 11, "male", 4000),
  new Person("sh", 22, "male", 4200),
  new Person("hd", 5, "female", 2000),
];

console.log(array)
console.log(Person.sort(array, "salary", "asc"));
